import matplotlib.pyplot as plt
import requests
import datetime
import pandas
 
# Get data on only confirmed cases
api_response = requests.get('https://covid19api.herokuapp.com/confirmed')

def daterange(date1, date2):
    for n in range(int ((date2 - date1).days)+1):
        yield date1 + datetime.timedelta(n)

start_dt = datetime.date(2020, 1, 22)
end_dt = datetime.date.today()
country = 'Lithuania'
for countryData in api_response.json()['locations']:
    if countryData['country'] == country:
        print(countryData['history'])
        previousDayTotal = 0
        dailyCaseList = []
        dailyIncreaseList = []
        for dt in daterange(start_dt, end_dt):
            datestring = "{dt.month}/{dt.day}/{dt:%g}".format(dt=dt)
            if datestring in countryData['history']:
                totalToday =  countryData['history'][datestring]
                newCases = totalToday - previousDayTotal
                print(end_dt)
                print(datestring, totalToday, 'delta: ', newCases)
                dailyCaseList.append(totalToday)
                dailyIncreaseList.append(newCases)
                previousDayTotal = totalToday

        y_mean = (pandas.DataFrame(dailyIncreaseList)).rolling(4).mean() 
        print(y_mean)
        plt.plot(dailyCaseList, dailyIncreaseList, 'bs', dailyCaseList, y_mean, 'r--')
        plt.title(country)
        plt.xlabel('Total cases')
        plt.ylabel('New cases')
        plt.xscale('log')
        plt.yscale('log')
        plt.grid(True)
        plt.show()
